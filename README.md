
# Transbank Module

This module integrates Transbank services using the official SDK.

# Requirements

This module require some php extensions
* curl
* json
* mbstring

# Dependencies
* Transbank PHP SDK (Include in composer.json):
  https://github.com/TransbankDevelopers/transbank-sdk-php

# Installation

Its recommend to download this module with composer https://getcomposer.org/.
