<?php

namespace Drupal\transbank_service_test\Plugin\TransbankServiceType;

use Drupal\transbank\TransbankServiceTypePluginBase;

/**
 * Test service type.
 *
 * @TransbankServiceType(
 *   id = "transbank_service_test_with_test_form_handler",
 *   label = @Translation("Transbank Service Test with Test Form"),
 *   forms = {
 *     "test" = "\Drupal\transbank_service_test\PluginForm\TransbankServiceTest\TestForm"
 *   }
 * )
 */
class TestServiceTypeWithTestFormHandler extends TransbankServiceTypePluginBase {}
