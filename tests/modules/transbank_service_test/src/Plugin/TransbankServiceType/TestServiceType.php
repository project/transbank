<?php

namespace Drupal\transbank_service_test\Plugin\TransbankServiceType;

use Drupal\transbank\TransbankServiceTypePluginBase;

/**
 * Test service type.
 *
 * @TransbankServiceType(
 *   id = "transbank_service_test",
 *   label = @Translation("Transbank Service Test"),
 * )
 */
class TestServiceType extends TransbankServiceTypePluginBase {}
