<?php

namespace Drupal\transbank_service_test\PluginForm\TransbankServiceTest;

use Drupal\Core\Form\FormStateInterface;
use Drupal\transbank\ServiceTypeTestFormBase;

/**
 *
 */
class TestForm extends ServiceTypeTestFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

}
