<?php

namespace Drupal\Tests\transbank\Kernel;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\transbank\Entity\Service;

/**
 * @coversDefaultClass \Drupal\transbank\ServiceAccessControlHandler
 * @group transbank
 */
class ServiceAccessControlHandlerTest extends KernelTestBase {

  use ProphecyTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'transbank',
    'transbank_service_test',
    'system',
    'user',
  ];

  /**
   * The service access control handler.
   *
   * @var \Drupal\transbank\ServiceAccessControlHandler
   */
  protected $accessControlHandler;

  /**
   * A test admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * A non-privileged user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('transbank_service');
    $this->installEntitySchema('user');
    $this->installSchema('system', ['sequences']);

    $this->accessControlHandler = $this->container->get('entity_type.manager')->getAccessControlHandler('transbank_service');

    // Create and discard user 1, which is special and bypasses all access
    // checking.
    $this->createUser([]);
    $this->user = $this->createUser([]);
    $this->adminUser = $this->createUser(['administer transbank service']);
  }

  /**
   * @covers ::checkCreateAccess
   */
  public function testCheckCreateAccess() {
    // A user must have the correct permission to create a workflow.
    $this->assertEquals(
      AccessResult::neutral()
        ->addCacheContexts(['user.permissions'])
        ->setReason("The 'administer transbank service' permission is required."),
      $this->accessControlHandler->createAccess(NULL, $this->user, [], TRUE)
    );
    $this->assertEquals(
      AccessResult::allowed()
        ->addCacheContexts(['user.permissions']),
      $this->accessControlHandler->createAccess(NULL, $this->adminUser, [], TRUE)
    );
  }

  /**
   * @covers ::checkAccess
   * @dataProvider checkAccessProvider
   */
  public function testCheckAccess($user, $operation, $result, $with_test_form) {
    $type = 'transbank_service_test_with_test_form_handler';
    if (!$with_test_form) {
      $type = 'transbank_service_test';
    }
    $service = Service::create([
      'type' => $type,
      'id' => 'test',
    ]);
    $service->save();

    if ($operation == 'test') {
      $result->addCacheableDependency($service);
    }

    $this->assertEquals($result, $this->accessControlHandler->access($service, $operation, $this->{$user}, TRUE));
  }

  /**
   * Data provider for ::testCheckAccess.
   *
   * @return array
   */
  public function checkAccessProvider() {
    $container = new ContainerBuilder();
    $cache_contexts_manager = $this->prophesize(CacheContextsManager::class);
    $cache_contexts_manager->assertValidTokens()->willReturn(TRUE);
    $cache_contexts_manager->reveal();
    $container->set('cache_contexts_manager', $cache_contexts_manager);
    \Drupal::setContainer($container);

    return [
      'Admin view' => [
        'adminUser',
        'view',
        AccessResult::allowed()->addCacheContexts(['user.permissions']),
        FALSE,
      ],
      'Admin update' => [
        'adminUser',
        'update',
        AccessResult::allowed()->addCacheContexts(['user.permissions']),
        FALSE,
      ],
      'Admin delete' => [
        'adminUser',
        'delete',
        AccessResult::allowed()->addCacheContexts(['user.permissions']),
        FALSE,
      ],
      'Admin test without test form handler' => [
        'adminUser',
        'test',
        AccessResult::forbidden(),
        FALSE,
      ],
      'Admin test with test form handler' => [
        'adminUser',
        'test',
        AccessResult::allowed()->addCacheContexts(['user.permissions']),
        TRUE,
      ],
      'User view' => [
        'user',
        'view',
        AccessResult::neutral()
          ->addCacheContexts(['user.permissions'])
          ->setReason("The 'administer transbank service' permission is required."),
        FALSE,
      ],
      'User update' => [
        'user',
        'update',
        AccessResult::neutral()
          ->addCacheContexts(['user.permissions'])
          ->setReason("The 'administer transbank service' permission is required."),
        FALSE,
      ],
      'User delete' => [
        'user',
        'delete',
        AccessResult::neutral()
          ->addCacheContexts(['user.permissions'])
          ->setReason("The 'administer transbank service' permission is required."),
        FALSE,
      ],
      'User test without test form handle' => [
        'user',
        'test',
        AccessResult::forbidden(),
        FALSE,
      ],
      'User test with test form handler' => [
        'user',
        'test',
        AccessResult::neutral()
          ->addCacheContexts(['user.permissions'])
          ->setReason("The 'administer transbank service' permission is required."),
        TRUE,
      ],
    ];
  }

}
