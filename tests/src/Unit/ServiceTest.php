<?php

namespace Drupal\Tests\transbank\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\transbank\Entity\Service;
use Drupal\transbank\TransbankServiceTypeInterface;
use Drupal\transbank\TransbankServiceTypePluginManager;
use Drupal\transbank_service_test\Plugin\TransbankServiceType\TestServiceType;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\transbank\TransbankServiceTypePluginBase
 *
 * @group transbank
 */
class ServiceTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = new ContainerBuilder();
    $workflow_manager = $this->prophesize(TransbankServiceTypePluginManager::class);
    $workflow_manager->createInstance('transbank_service_test', Argument::any())->willReturn(new TestServiceType([], '', []));
    $container->set('plugin.manager.transbank_service_type', $workflow_manager->reveal());
    \Drupal::setContainer($container);
  }

  /**
   * Test the service instance.
   */
  public function testServiceInstance() {
    $data = [
      'id' => 'test',
      'label' => 'Test',
      'type' => 'transbank_service_test',
      'service_settings' => [
        'commerce_code' => '123',
        'environment' => 'TEST',
        'api_key' => '123',
      ],
    ];
    $service = new Service($data, 'transbank_service');

    $this->assertInstanceOf(TransbankServiceTypeInterface::class, $service->getServiceInstance());
    $this->assertEquals([
      'commerce_code' => '123',
      'environment' => 'TEST',
      'api_key' => '123',
    ], $service->getServiceInstance()->getConfiguration());
  }

}
