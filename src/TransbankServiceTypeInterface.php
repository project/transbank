<?php

namespace Drupal\transbank;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;

/**
 * Interface for transbank_service plugins.
 */
interface TransbankServiceTypeInterface extends ConfigurableInterface, PluginFormInterface, PluginWithFormsInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

}
