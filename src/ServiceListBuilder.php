<?php

namespace Drupal\transbank;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of options.
 */
class ServiceListBuilder extends ConfigEntityListBuilder {

  protected TransbankServiceTypePluginManagerInterface $serviceTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);

    $instance->serviceTypeManager = $container->get('plugin.manager.transbank_service_type');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['type'] = $this->t('Service Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\transbank\ServiceInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();

    try {
      $definition = $this->serviceTypeManager->getDefinition($entity->get('type'));
      $row['type'] = $definition['label'];
    }
    catch (PluginNotFoundException $e) {
      $row['type'] = $this->t('Service type @service_type not found', [
        '@service_type' => $entity->get('type'),
      ]);
    }
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    if ($entity->access('test')) {
      $operations['test'] = [
        'title' => $this->t('Test'),
        'url' => $entity->toUrl('test'),
      ];
    }

    return $operations + parent::getDefaultOperations($entity);
  }

}
