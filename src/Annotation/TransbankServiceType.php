<?php

namespace Drupal\transbank\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines transbank_service annotation object.
 *
 * @Annotation
 */
class TransbankServiceType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * @var string
   */
  public $test = '';

}
