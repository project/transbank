<?php

namespace Drupal\transbank;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 *
 */
class ServiceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'test') {
      /** @var \Drupal\transbank\ServiceInterface $entity */
      try {
        $plugin = $entity->getServiceInstance();
        if ($plugin->hasFormClass('test')) {
          return AccessResult::allowedIfHasPermission($account, 'administer transbank service')
            ->addCacheableDependency($entity);
        }
      }
      catch (PluginException $e) {
      }
      return AccessResult::forbidden()
        ->addCacheableDependency($entity);
    }

    return parent::checkAccess($entity, $operation, $account);
  }

}
