<?php

namespace Drupal\transbank;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Transbank service entities.
 */
class ServiceHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $route_edit = $collection->get('entity.transbank_service.edit_form');
    $collection->add('entity.transbank_service.canonical', $route_edit);

    return $collection;
  }

}
