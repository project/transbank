<?php

namespace Drupal\transbank\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\transbank\TransbankServiceTypePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
trait ServicePaymentGatewayTrait {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $serviceStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->serviceStorage = $instance->entityTypeManager->getStorage('transbank_service');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedModes() {
    return ['n/a' => $this->t('N/A')] + TransbankServiceTypePluginBase::environments();
  }

  /**
   * Gets an array of services of a type.
   *
   * @param string $type
   *   The service type.
   *
   * @return array
   *   The array of services.
   */
  protected function getServicesOptions(string $type): array {
    $options = [];

    $services = $this->serviceStorage->loadByProperties([
      'type' => $type,
    ]);
    foreach ($services as $service) {
      $options[$service->id()] = $service->label();
    }

    return $options;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param string $type
   *
   * @return array
   */
  protected function buildConfigurationFormType(array $form, FormStateInterface $form_state, string $type): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['service'] = [
      '#type' => 'select',
      '#title' => $this->t('Service'),
      '#options' => $this->getServicesOptions($type),
      '#required' => TRUE,
      '#default_value' => $this->configuration['service'],
    ];

    // The mode is defined by the service.
    $form['mode']['#access'] = FALSE;

    return $form;
  }

}
