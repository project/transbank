<?php

namespace Drupal\transbank\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\transbank\ServiceInterface;
use Drupal\transbank\TransbankServiceTypeInterface;

/**
 * Defines the service entity type.
 *
 * @ConfigEntityType(
 *   id = "transbank_service",
 *   label = @Translation("Service"),
 *   label_collection = @Translation("Services"),
 *   label_singular = @Translation("service"),
 *   label_plural = @Translation("services"),
 *   label_count = @PluralTranslation(
 *     singular = "@count service",
 *     plural = "@count services",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\transbank\ServiceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\transbank\Form\ServiceForm",
 *       "edit" = "Drupal\transbank\Form\ServiceForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\transbank\ServiceHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\transbank\ServiceAccessControlHandler",
 *   },
 *   config_prefix = "transbank_service",
 *   admin_permission = "administer transbank service",
 *   links = {
 *     "canonical" = "/admin/config/services/transbank/service/{transbank_service}",
 *     "collection" = "/admin/config/services/transbank/service",
 *     "add-form" = "/admin/config/services/transbank/service/add",
 *     "edit-form" = "/admin/config/services/transbank/service/{transbank_service}",
 *     "delete-form" = "/admin/config/services/transbank/service/{transbank_service}/delete",
 *     "test" = "/admin/config/services/transbank/service/{transbank_service}/test",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "type",
 *     "service_settings",
 *   }
 * )
 */
class Service extends ConfigEntityBase implements ServiceInterface {

  /**
   * The service ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The service label.
   *
   * @var string
   */
  protected $label;

  /**
   * The transbank_service description.
   *
   * @var string
   */
  protected $description;

  /**
   * @var string
   */
  protected $type;

  /**
   * @var array
   */
  protected $service_settings = [];

  /**
   * The service type plugin collection.
   *
   * @var \Drupal\Component\Plugin\LazyPluginCollection
   */
  protected $pluginCollection;

  /**
   * {@inheritdoc}
   */
  public function getServiceType(): string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceSettings(): array {
    return $this->service_settings ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getServiceInstance(): TransbankServiceTypeInterface {
    return $this->getPluginCollection()->get($this->type);
  }

  /**
   * Encapsulates the creation of the service type's plugin collection.
   *
   * @return \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   *   The service type's plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection && $this->type) {
      $this->pluginCollection = new DefaultSingleLazyPluginCollection(\Drupal::service('plugin.manager.transbank_service_type'), $this->type, $this->service_settings);
    }
    return $this->pluginCollection;
  }

}
