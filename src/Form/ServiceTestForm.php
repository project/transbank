<?php

namespace Drupal\transbank\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\transbank\ServiceInterface;
use Drupal\transbank\TransbankServiceTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 */
class ServiceTestForm extends FormBase {

  /**
   * @var \Drupal\Core\Plugin\PluginFormFactoryInterface
   */
  protected PluginFormFactoryInterface $pluginFormFactory;

  /**
   * @var \Drupal\transbank\TransbankServiceTypeInterface
   */
  protected TransbankServiceTypeInterface $servicePluginInstance;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pluginFormFactory = $container->get('plugin_form.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'transbank_service_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ServiceInterface $transbank_service = NULL) {
    $this->servicePluginInstance = $transbank_service->getServiceInstance();
    $form_state->set('service', $transbank_service);

    return $this->pluginFormFactory->createInstance($this->servicePluginInstance, 'test')->buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->pluginFormFactory->createInstance($this->servicePluginInstance, 'test')->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->pluginFormFactory->createInstance($this->servicePluginInstance, 'test')->submitConfigurationForm($form, $form_state);
  }

}
