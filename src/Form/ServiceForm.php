<?php

namespace Drupal\transbank\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\transbank\TransbankServiceTypePluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Option form.
 *
 * @property \Drupal\transbank\ServiceInterface $entity
 */
class ServiceForm extends EntityForm {

  /**
   * @var \Drupal\transbank\TransbankServiceTypePluginManagerInterface
   */
  protected TransbankServiceTypePluginManagerInterface $serviceTypeManager;

  /**
   * @param \Drupal\transbank\TransbankServiceTypePluginManagerInterface $service_manager
   */
  public function __construct(TransbankServiceTypePluginManagerInterface $service_manager) {
    $this->serviceTypeManager = $service_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.transbank_service_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the service.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\transbank\Entity\Service::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the service.'),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Service'),
      '#default_value' => $this->entity->get('type'),
      '#options' => $this->serviceTypeManager->getOptions(),
      '#required' => TRUE,
      '#ajax' => [
        'wrapper' => 'service-settings-wrapper',
        'callback' => [$this, 'updateServiceSettingsAjax'],
      ],
    ];

    $form['service_settings'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $this->t('Service Settings'),
      '#prefix' => '<div id="service-settings-wrapper">',
      '#suffix' => '</div>',
    ];
    $service_type = $form_state->getValue('type') ?? $this->entity->getServiceType();

    if ($service_type) {
      $instance = $this->serviceTypeManager->createInstance($service_type, $this->entity->getServiceSettings());
      $subform_state = SubformState::createForSubform($form['service_settings'], $form, $form_state);
      $form['service_settings'] = $instance->buildConfigurationForm($form['service_settings'], $subform_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $service_id = $form_state->getValue('type');
    $instance = $this->serviceTypeManager->createInstance($service_id, $this->entity->get('service_settings'));
    $subform_state = SubformState::createForSubform($form['service_settings'], $form, $form_state);
    $instance->validateConfigurationForm($form, $subform_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $service_id = $form_state->getValue('type');
    $instance = $this->serviceTypeManager->createInstance($service_id, $this->entity->get('service_settings'));
    $subform_state = SubformState::createForSubform($form['service_settings'], $form, $form_state);
    $instance->submitConfigurationForm($form, $subform_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new service %label.', $message_args)
      : $this->t('Updated service %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function updateServiceSettingsAjax($form, FormStateInterface $form_state) {
    return $form['service_settings'];
  }

}
