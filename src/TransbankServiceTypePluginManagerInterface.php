<?php

namespace Drupal\transbank;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 *
 */
interface TransbankServiceTypePluginManagerInterface extends PluginManagerInterface {

  /**
   * @return string[]
   */
  public function getOptions();

}
