<?php

namespace Drupal\transbank;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * TransbankService plugin manager.
 */
class TransbankServiceTypePluginManager extends DefaultPluginManager implements TransbankServiceTypePluginManagerInterface {

  /**
   * Constructs TransbankServicePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/TransbankServiceType',
      $namespaces,
      $module_handler,
      'Drupal\transbank\TransbankServiceTypeInterface',
      'Drupal\transbank\Annotation\TransbankServiceType'
    );
    $this->alterInfo('transbank_service_type_info');
    $this->setCacheBackend($cache_backend, 'transbank_service_type_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions() {
    $options = [];
    foreach ($this->getDefinitions() as $definition) {
      $options[$definition['id']] = $definition['label'];
    }

    return $options;
  }

}
