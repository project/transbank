<?php

namespace Drupal\transbank;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Transbank\Webpay\Options;

/**
 * Base class for transbank_service plugins.
 */
abstract class TransbankServiceTypePluginBase extends PluginBase implements TransbankServiceTypeInterface {

  use StringTranslationTrait;
  use PluginWithFormsTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'commerce_code' => '',
      'api_key' => '',
      'environment' => Options::ENVIRONMENT_INTEGRATION,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['commerce_code'] = [
      '#type' => 'textfield',
      '#maxlength' => 12,
      '#size' => 12,
      '#title' => $this->t('Commerce Code'),
      '#default_value' => $this->configuration['commerce_code'],
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Api key'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    $form['environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#options' => self::environments(),
      '#default_value' => $this->configuration['environment'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['commerce_code'] = $form_state->getValue('commerce_code');
    $this->configuration['api_key'] = $form_state->getValue('api_key');
    $this->configuration['environment'] = $form_state->getValue('environment');
  }

  /**
   * Get the environments options.
   *
   * @return array
   *   The environments options.
   */
  public static function environments() {
    return [
      Options::ENVIRONMENT_INTEGRATION => t('Testing/Integration'),
      Options::ENVIRONMENT_PRODUCTION => t('Production'),
    ];
  }

  /**
   * @return \Transbank\Webpay\Options
   */
  protected function getOptions(): Options {
    return new Options(
      $this->configuration['api_key'],
      $this->configuration['commerce_code'],
      $this->configuration['environment']
    );
  }

}
