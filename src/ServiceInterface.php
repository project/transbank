<?php

namespace Drupal\transbank;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an service entity type.
 */
interface ServiceInterface extends ConfigEntityInterface {

  /**
   * @return string
   */
  public function getServiceType(): string;

  /**
   * @return array
   */
  public function getServiceSettings(): array;

  /**
   * @return \Drupal\transbank\TransbankServiceTypeInterface
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getServiceInstance(): TransbankServiceTypeInterface;

}
