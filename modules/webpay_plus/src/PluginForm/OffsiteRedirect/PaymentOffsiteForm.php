<?php

namespace Drupal\transbank_webpay_plus\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\transbank\Entity\Service;
use Drupal\commerce_payment\Entity\Payment;
use GuzzleHttp\Exception\GuzzleException;
use Transbank\Webpay\WebpayPlus\Exceptions\TransactionCreateException;

/**
 * Class PaymentOffsiteForm.
 *
 * @package Drupal\transbank_webpay_plus\PluginForm\OffsiteRedirect
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $payment = $this->entity;
    $orderId = $payment->getOrderId();
    $amount = $payment->getAmount()->getNumber();
    $paymentGateway = $payment->getPaymentGateway();
    $payment_gateway_plugin = $paymentGateway->getPlugin();
    $service_id = $payment_gateway_plugin->getConfiguration()['service'];

    /** @var \Drupal\transbank\ServiceInterface $service */
    if (!($service = Service::load($service_id))) {
      throw new \InvalidArgumentException('The service does not exist.');
    }

    /** @var \Drupal\transbank_webpay_plus\Plugin\TransbankServiceType\WebpayPlus $instance */
    $instance = $service->getServiceInstance();
    $transaction = $instance->getTransaction();

    $payment = Payment::create([
      'state' => 'new',
      'amount' => $payment->getAmount(),
      'payment_gateway' => $paymentGateway->id(),
      'order_id' => $orderId,
    ]);
    $payment->save();

    try {
      $response = $transaction->create($orderId, $payment->id(), $amount, $form['#return_url']);
      $redirect_method = self::REDIRECT_POST;
      $data = [
        'return' => $form['#return_url'],
        'cancel' => $form['#cancel_url'],
        'token_ws' => $response->token,
        'total' => $payment->getAmount()->getNumber(),
      ];

      return $this->buildRedirectForm($form, $form_state, $response->url, $data, $redirect_method);
    }
    catch (GuzzleException | TransactionCreateException $e) {
      $payment->delete();
    }

    return parent::buildConfigurationForm($form, $form_state);
  }

}
