<?php

namespace Drupal\transbank_webpay_plus\PluginForm\TransbankServiceTest;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\transbank\ServiceTypeTestFormBase;
use Drupal\transbank_webpay_plus\Plugin\TransbankServiceType\WebpayPlusInterface;
use GuzzleHttp\Exception\GuzzleException;
use Transbank\Webpay\WebpayPlus\Exceptions\TransactionCommitException;
use Transbank\Webpay\WebpayPlus\Exceptions\TransactionCreateException;

/**
 *
 */
class WebpayPlusTestForm extends ServiceTypeTestFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $request = $this->getRequest();

    $service = $form_state->get('service');
    /** @var \Drupal\transbank_webpay_plus\Plugin\TransbankServiceType\WebpayPlusInterface $service_plugin */
    $service_plugin = $this->plugin;

    $form['information'] = [
      '#type' => 'container',
    ];

    if ($this->isIntroduction($form_state)) {
      $form_state->set('step', 'init');

      $form['information']['introduction'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('This section can help you to test the connection with Webpay. Press "Test connection" to start.'),
      ];

      $form['init'] = [
        '#type' => 'submit',
        '#value' => $this->t('Test connection'),
      ];
    }
    elseif ($this->isReadyToGo($form_state)) {
      $data_test = $form_state->get('data_test');
      $amount = $data_test['amount'];
      $order_number = $data_test['order_number'];

      $return_url = Url::fromRoute('entity.transbank_service.test', [
        'end' => 'end',
        'transbank_service' => $service->id(),
      ])->setAbsolute();

      try {
        $transaction = $service_plugin->getTransaction();
        $response = $transaction->create($order_number, uniqid(), $amount, $return_url->toString());

        $form['information']['success'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('The site successfully connected to Webpay. Now you can do the transaction. Press "Go to Webpay".'),
        ];
        $form['information']['data'] = [
          '#theme' => 'item_list',
          '#items' => [
            $this->t('Amount: $:amount', [':amount' => number_format($amount, 0)]),
            $this->t('Order Number: :order_number', [':order_number' => $order_number]),
          ],
        ];
        $form['#action'] = $response->getUrl();
        $form['token_ws'] = [
          '#type' => 'hidden',
          '#value' => $response->getToken(),
        ];
        $form['go'] = [
          '#type' => 'submit',
          '#value' => $this->t('Go to Webpay'),
        ];

      }
      catch (GuzzleException $e) {
        $form['information']['problem'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Exists some problems with the connection with Webpay.'),
        ];
      }
      catch (TransactionCreateException $e) {
        $form['information']['problem'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Exists some problems with the connection with Webpay: %error', [
            '%error' => $e->getMessage(),
          ]),
        ];
      }
    }
    elseif ($this->isTransbankResponse($form_state)) {
      if (!$service_plugin->isValidRequest($request)) {
        $form['information']['problem'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => $this->t('Exists some problems with the connection with Webpay.'),
        ];
      }
      else {
        $token = $request->query->get('token_ws');
        try {
          $transaction = $service_plugin->getTransaction();
          $response = $transaction->commit($token);
          $form['information']['response'] = [
            '#type' => 'html_tag',
            '#tag' => 'pre',
            '#value' => print_r($response, TRUE),
          ];
        }
        catch (GuzzleException $e) {
          $form['information']['problem'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $this->t('Exists some problems with the connection with Transbank.'),
          ];
        }
        catch (TransactionCommitException $e) {
          $form['information']['problem'] = [
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $this->t('Exists some problems with the connection with Transbank: %error', [
              '%error' => $e->getMessage(),
            ]),
          ];
        }
      }
      $form['init'] = [
        '#type' => 'submit',
        '#value' => $this->t('Try again'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form_state->set('step', 'go');
    $form_state->setRebuild();

    $form_state->set('data_test', [
      'amount' => rand(10000, 150000),
      'order_number' => rand(1, 50000),
    ]);
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  protected function isIntroduction(FormStateInterface $form_state): bool {
    $step = $form_state->get('step');

    return empty($step) && !$this->isTransbankResponse($form_state);
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  protected function isReadyToGo(FormStateInterface $form_state): bool {
    $step = $form_state->get('step');

    return $step == 'go';
  }

  /**
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return bool
   */
  protected function isTransbankResponse(FormStateInterface $form_state): bool {
    $request = $this->getRequest();
    $end = $request->query->get('end');
    $step = $form_state->get('step');

    return $end && !$step;
  }

}
