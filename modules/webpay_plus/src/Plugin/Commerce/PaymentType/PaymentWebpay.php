<?php

namespace Drupal\transbank_webpay_plus\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the manual payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_webpay",
 *   label = @Translation("Webpay"),
 *   workflow = "payment_default",
 * )
 */
class PaymentWebpay extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['webpay_plus_vci'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('VCI'))
      ->setDescription($this->t('The vci of the transaction.'))
      ->setSetting('max_length', 4);
    $fields['webpay_plus_authorization_code'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('authorization code'))
      ->setDescription($this->t('The authorization_code of the transaction.'))
      ->setSetting('max_length', 6);
    $fields['webpay_plus_payment_type_code'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Payment type code'))
      ->setDescription($this->t('The payment type code of the transaction.'))
      ->setSetting('max_length', 4);
    $fields['webpay_plus_card_number'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Card number'))
      ->setDescription($this->t("Last 4 numbers of the cardholder's credit card. Only for businesses authorized by Transbank the complete number is sent."))
      ->setSetting('max_length', 19);
    $fields['webpay_plus_response_code'] = BundleFieldDefinition::create('integer')
      ->setLabel($this->t('Response code'));

    return $fields;
  }

}
