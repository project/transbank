<?php

namespace Drupal\transbank_webpay_plus\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\transbank\Entity\Service;
use Drupal\transbank\Plugin\Commerce\PaymentGateway\ServicePaymentGatewayTrait;
use Drupal\transbank_webpay_plus\Plugin\TransbankServiceType\WebpayPlusInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Transbank\Webpay\WebpayPlus\Exceptions\TransactionCaptureException;
use Transbank\Webpay\WebpayPlus\Exceptions\TransactionCommitException;
use Transbank\Webpay\WebpayPlus\Exceptions\TransactionRefundException;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "transbank_webpay_plus",
 *   label = "Webpay Plus",
 *   display_label = "Webpay Plus",
 *   forms = {
 *     "offsite-payment" = "Drupal\transbank_webpay_plus\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   requires_billing_information = FALSE,
 *   payment_type = "payment_webpay",
 * )
 */
class WebpayPlus extends OffsitePaymentGatewayBase implements SupportsRefundsInterface, SupportsAuthorizationsInterface {

  use ServicePaymentGatewayTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'service' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $this->buildConfigurationFormType($form, $form_state, 'webpay_plus');
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\transbank\ServiceInterface $service */
    $service = $this->serviceStorage->load($values['service']);
    $this->configuration['service'] = $service->id();
    $this->configuration['mode'] = $service->getServiceSettings()['environment'];
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $token = $request->query->get('token_ws');

    $service_id = $this->configuration['service'];
    $service = $this->serviceStorage->load($service_id);

    /** @var \Drupal\transbank_webpay_plus\Plugin\TransbankServiceType\WebpayPlus $instance */
    $instance = $service->getServiceInstance();

    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    if ($instance->isValidRequest($request)) {
      $transaction = $instance->getTransaction();
      try {
        if ($token && ($response = $transaction->commit($token))) {
          /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
          if ($payment = $payment_storage->load($response->getSessionId())) {
            if ($payment->getState()->getId() != 'new') {
              return;
            }
            if ($response->isApproved()) {
              $new_state = 'authorization';
              if ($response->getPaymentTypeCode() == 'VD' || $response->getPaymentTypeCode() == 'VP') {
                $new_state = 'completed';
              }
              if ($response->getStatus() == WebpayPlusInterface::STATUS_AUTHORIZED) {
                $payment->set('authorized', $this->time->getCurrentTime());
              }
            }
            else {
              $new_state = 'authorization_voided';
            }
            $payment->set('webpay_plus_vci', $response->getVci());
            $payment->set('webpay_plus_authorization_code', $response->getAuthorizationCode());
            $payment->set('webpay_plus_payment_type_code', $response->getPaymentTypeCode());
            $payment->set('webpay_plus_card_number', $response->getCardNumber());
            $payment->set('webpay_plus_response_code', $response->getResponseCode());
            $payment->setState($new_state);
            $payment->setRemoteId($token);
            $payment->setRemoteState($response->getStatus());

            $payment->save();

            if ($new_state != 'authorization_voided') {
              return;
            }
          }
        }
      }
      catch (GuzzleException | TransactionCommitException $e) {
      }
    }
    else {
      $session_id = $request->query->get('TBK_ID_SESION');
      if ($session_id && ($payment = $payment_storage->load($session_id))) {
        $payment->setState('voided');
        $payment->save();
      }
    }

    throw new PaymentGatewayException($this->t('We encountered an unexpected error processing your payment method. Please try again later.'));
  }

  /**
   * {@inheritdoc}
   */
  public function canRefundPayment(PaymentInterface $payment) {
    if (!parent::canRefundPayment($payment)) {
      return FALSE;
    }

    if ($payment->getCompletedTime() &&
      $payment->getCompletedTime() < strtotime('-3 hours')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    $transaction = $this->getWebpayTransaction($payment);
    try {
      $response = $transaction->refund($payment->getRemoteId(), $amount->getNumber());
      if ($response->success()) {
        if ($response->getType() == 'REVERSED') {
          $new_remote_state = WebpayPlusInterface::STATUS_REVERSED;
        }
        else {
          $new_remote_state = WebpayPlusInterface::STATUS_NULLIFIED;
        }
        $old_refunded_amount = $payment->getRefundedAmount();
        $new_refunded_amount = $old_refunded_amount->add($amount);
        if ($new_refunded_amount->lessThan($payment->getAmount())) {
          $new_remote_state = WebpayPlusInterface::STATUS_PARTIALLY_NULLIFIED;
          $payment->setState('partially_refunded');
        }
        else {
          $payment->setState('refunded');
        }
        $payment->setRemoteState($new_remote_state);
        $payment->setRefundedAmount($new_refunded_amount);
        $payment->save();
      }
      else {
        throw new PaymentGatewayException($this->t('Refund failed with code @code', ['@code' => $response->getResponseCode()]));
      }
    }
    catch (GuzzleException | TransactionRefundException $e) {
      throw new PaymentGatewayException($this->t('Refund failed: @reason', ['@reason' => Markup::create($e->getMessage())]), $e->getCode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    $amount = $amount ?: $payment->getAmount();

    $transaction = $this->getWebpayTransaction($payment);
    try {
      $response = $transaction->capture(
        $payment->getRemoteId(),
        $payment->getOrderId(),
        $payment->get('webpay_plus_authorization_code')->value,
        $amount->getNumber()
      );

      if ($response->isApproved()) {
        $payment->setAmount($amount);
        $payment->setState('completed');
        $payment->setRemoteState(WebpayPlusInterface::STATUS_CAPTURED);
        $payment->save();
      }
      else {
        throw new PaymentGatewayException($this->t('Capture failed with code @code', ['@code' => $response->getResponseCode()]));
      }
    }
    catch (GuzzleException | TransactionCaptureException $e) {
      throw new PaymentGatewayException($this->t('Capture failed: @reason', ['@reason' => Markup::create($e->getMessage())]), $e->getCode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function canVoidPayment(PaymentInterface $payment) {
    if (!in_array($payment->getState()->getId(), ['completed', 'authorization'], TRUE)) {
      return FALSE;
    }

    if ($payment->getCompletedTime() &&
      $payment->getCompletedTime() < strtotime('-90 days')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization', 'completed']);
    $transaction = $this->getWebpayTransaction($payment);
    $amount = $payment->getAmount();
    try {
      $response = $transaction->refund($payment->getRemoteId(), $amount->getNumber());
      if ($response->success()) {
        $payment->setState('authorization_voided');
        $payment->setRemoteState(WebpayPlusInterface::STATUS_NULLIFIED);
        $payment->save();
      }
      else {
        throw new PaymentGatewayException($this->t('Void failed with code @code', ['@code' => $response->getResponseCode()]));
      }
    }
    catch (GuzzleException | TransactionRefundException $e) {
      throw new PaymentGatewayException($this->t('Void failed: @reason', ['@reason' => Markup::create($e->getMessage())]), $e->getCode());
    }
  }

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @return \Transbank\Webpay\WebpayPlus\MallTransaction|\Transbank\Webpay\WebpayPlus\Transaction
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getWebpayTransaction(PaymentInterface $payment) {
    $configuration = $payment->getPaymentGateway()->getPluginConfiguration();
    $service_id = $configuration['service'] ?? NULL;

    if (!$service_id || !($service = Service::load($service_id))) {
      throw new \InvalidArgumentException('The service webpay does not exists.');
    }

    /** @var \Drupal\transbank_webpay_plus\Plugin\TransbankServiceType\WebpayPlusInterface $instance */
    $instance = $service->getServiceInstance();
    return $instance->getTransaction();
  }

}
