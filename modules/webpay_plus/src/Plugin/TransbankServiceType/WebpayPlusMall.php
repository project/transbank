<?php

namespace Drupal\transbank_webpay_plus\Plugin\TransbankServiceType;

use Transbank\Webpay\Options;
use Transbank\Webpay\WebpayPlus\MallTransaction;

/**
 * @TransbankServiceType(
 *   id = "webpay_plus_mall",
 *   label = @Translation("Webpay Plus Mall"),
 *   description = @Translation("Webpay Plus Mall allows you to make a financial authorization request for a set of payments with credit, debit or prepaid cards, through a single unified transaction. Who makes the payment, enters the merchant's site, selects several products or services (belonging to different merchant codes), and selects Webpay Plus as a means of payment, making the transaction securely in Webpay for all payments. Each payment will have its own result, authorized or rejected. The 'mall' concept brings together multiple stores. It is these stores that can generate transactions. Both the mall and each of the associated stores are identified by a different commercial code.")
 * )
 */
class WebpayPlusMall extends WebpayPlus {

  /**
   * {@inheritdoc}
   */
  public function getTransaction() {
    $options = new Options(
      $this->configuration['api_key'],
      $this->configuration['commerce_code'],
      $this->configuration['environment']
    );

    return new MallTransaction($options);
  }

}
