<?php

namespace Drupal\transbank_webpay_plus\Plugin\TransbankServiceType;

use Symfony\Component\HttpFoundation\Request;

/**
 *
 */
interface WebpayPlusInterface {

  const STATUS_INITIALIZED = 'INITIALIZED';
  const STATUS_AUTHORIZED = 'AUTHORIZED';
  const STATUS_REVERSED = 'REVERSED';
  const STATUS_FAILED = 'FAILED';
  const STATUS_NULLIFIED = 'NULLIFIED';
  const STATUS_PARTIALLY_NULLIFIED = 'PARTIALLY_NULLIFIED';
  const STATUS_CAPTURED = 'CAPTURED';

  const USER_ABORTED_PAYMENT = 'aborted';

  const SOME_ERROR_ON_WEBPAY = 'some_error';

  const USER_WAS_IDLE = 'user_was_idle';

  const IS_NOT_A_NORMAL_RESPONSE = 'is_invalid';

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return bool
   */
  public function isValidRequest(Request $request);

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return false|string
   */
  public function isInvalidRequest(Request $request);

  /**
   * @return \Transbank\Webpay\WebpayPlus\Transaction|\Transbank\Webpay\WebpayPlus\MallTransaction
   */
  public function getTransaction();

}
