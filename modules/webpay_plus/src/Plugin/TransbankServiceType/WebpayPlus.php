<?php

namespace Drupal\transbank_webpay_plus\Plugin\TransbankServiceType;

use Drupal\transbank\TransbankServiceTypePluginBase;
use Symfony\Component\HttpFoundation\Request;
use Transbank\Webpay\WebpayPlus\Transaction;

/**
 * @TransbankServiceType(
 *   id = "webpay_plus",
 *   label = @Translation("Webpay Plus"),
 *   description = @Translation("Webpay Plus allows you to make a financial authorization request for a payment with credit or debit cards where the person making the payment enters the merchant's site, selects products or services, and enters the data associated with the credit or debit card. Redcompra does it safely in Webpay Plus. The merchant that receives payments through Webpay Plus is identified by a commerce code."),
 *   forms = {
 *     "test" = "\Drupal\transbank_webpay_plus\PluginForm\TransbankServiceTest\WebpayPlusTestForm",
 *   },
 * )
 */
class WebpayPlus extends TransbankServiceTypePluginBase implements WebpayPlusInterface {

  /**
   * {@inheritdoc}
   */
  public function isValidRequest(Request $request) {
    return !$this->isInvalidRequest($request);
  }

  /**
   * {@inheritdoc}
   */
  public function isInvalidRequest(Request $request) {
    $token_ws = $request->query->get('token_ws');
    $tbk_token = $request->query->get('TBK_TOKEN');
    $buy_order = $request->query->get('TBK_ORDEN_COMPRA');
    $session_id = $request->query->get('TBK_ID_SESION');

    if ($tbk_token && $buy_order && $session_id && !$token_ws) {
      return self::USER_ABORTED_PAYMENT;
    }
    if ($token_ws && $buy_order && $session_id && $tbk_token) {
      return self::SOME_ERROR_ON_WEBPAY;
    }
    if ($buy_order && $session_id && !$token_ws && !$tbk_token) {
      return self::USER_WAS_IDLE;
    }
    if (!($token_ws && !$buy_order && !$session_id && !$tbk_token)) {
      return self::IS_NOT_A_NORMAL_RESPONSE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransaction() {
    return new Transaction($this->getOptions());
  }

}
