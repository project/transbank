<?php

namespace Drupal\transbank_complete_transaction\Plugin\TransbankServiceType;

use Drupal\transbank\TransbankServiceTypePluginBase;
use Transbank\TransaccionCompleta\Transaction;
use Transbank\Webpay\Options;

/**
 * @TransbankServiceType(
 *   id = "complete_transaction",
 *   label = @Translation("Complete Transaction"),
 *   description = @Translation("A Complete Transaction allows the merchant to present the cardholder with its own form to store the card data, expiration date and cvv (not necessary for merchants with the option without cvv enabled)."),
 * )
 */
class CompleteTransaction extends TransbankServiceTypePluginBase {

  /**
   * @return \Transbank\TransaccionCompleta\Transaction
   */
  public function getTransaction() {
    $options = new Options(
      $this->configuration['api_key'],
      $this->configuration['commerce_code'],
      $this->configuration['environment']
    );

    return new Transaction($options);
  }

}
