<?php

namespace Drupal\transbank_oneclick\Plugin\TransbankServiceType;

use Transbank\Webpay\Oneclick\MallInscription;
use Transbank\Webpay\Oneclick\MallTransaction;

/**
 * Defines the interface for oneclick service.
 */
interface OneclickInterface {

  /**
   * Create an instance of MallInscription.
   *
   * @return \Transbank\Webpay\Oneclick\MallInscription
   *   The MallInscription instance.
   */
  public function getInscription(): MallInscription;

  /**
   * Create an instance of MallTransaction.
   *
   * @return \Transbank\Webpay\Oneclick\MallTransaction
   *   The MallTransaction instance.
   */
  public function getTransaction(): MallTransaction;

}
