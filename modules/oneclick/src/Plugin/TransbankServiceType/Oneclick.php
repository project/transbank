<?php

namespace Drupal\transbank_oneclick\Plugin\TransbankServiceType;

use Drupal\transbank\TransbankServiceTypePluginBase;
use Transbank\Webpay\Oneclick\MallInscription;
use Transbank\Webpay\Oneclick\MallTransaction;

/**
 * Provides the oneclick service type.
 *
 * @TransbankServiceType(
 *   id = "oneclick",
 *   label = @Translation("Oneclick Mall"),
 *   description = @Translation("Oneclick Mall allows you to bundle payments into a single Oneclick transaction to multiple commerce codes (similar to a Mall transaction). In a transaction of this type, as in Oneclick, the cardholder will be able to make payments without having to enter their credit card data in each one of them. This type of payment facilitates the sale, centralizes payments, reduces transaction time and reduces the risk of erroneous data entry of the payment method.")
 * )
 */
class Oneclick extends TransbankServiceTypePluginBase implements OneclickInterface {

  /**
   * {@inheritdoc}
   */
  public function getInscription(): MallInscription {
    return new MallInscription($this->getOptions());
  }

  /**
   * {@inheritdoc}
   */
  public function getTransaction(): MallTransaction {
    return new MallTransaction($this->getOptions());
  }

}
