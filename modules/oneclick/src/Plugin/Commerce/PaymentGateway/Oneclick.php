<?php

namespace Drupal\transbank_oneclick\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\transbank\Entity\Service;
use Drupal\transbank\Plugin\Commerce\PaymentGateway\ServicePaymentGatewayTrait;
use Transbank\Webpay\Oneclick\Exceptions\InscriptionDeleteException;
use Transbank\Webpay\Oneclick\Exceptions\MallRefundTransactionException;
use Transbank\Webpay\Oneclick\Exceptions\MallTransactionAuthorizeException;
use Transbank\Webpay\Oneclick\Exceptions\MallTransactionCaptureException;

/**
 * Provides the Onsite payment oneclick.
 *
 * @CommercePaymentGateway(
 *   id = "transbank_oneclick",
 *   label = "Oneclick",
 *   display_label = "Oneclick",
 *   forms = {
 *     "add-payment-method" = "Drupal\transbank_oneclick\PluginForm\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"transbank_oneclick"},
 *   requires_billing_information = FALSE,
 * )
 */
class Oneclick extends OnsitePaymentGatewayBase implements OnsitePaymentGatewayInterface, SupportsAuthorizationsInterface, SupportsRefundsInterface {

  use ServicePaymentGatewayTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'service' => NULL,
      'child_commerce_code' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = $this->buildConfigurationFormType($form, $form_state, 'oneclick');

    $form['child_commerce_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Child commerce code'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['child_commerce_code'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\transbank\ServiceInterface $service */
    $service = $this->serviceStorage->load($values['service']);
    $this->configuration['service'] = $service->id();
    $this->configuration['child_commerce_code'] = $values['child_commerce_code'];
    $this->configuration['mode'] = $service->getServiceSettings()['environment'];
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
    $instance = $this->getServiceInstanceFromPaymentMethod($payment_method);
    $inscription = $instance->getInscription();

    try {
      $inscription->delete($payment_method->get('oneclick_local_username')->value, $payment_method->get('oneclick_tbk_user')->value);
      $payment_method->delete();
    }
    catch (InscriptionDeleteException $e) {
      throw new PaymentGatewayException($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
    $instance = $this->getServiceInstanceFromPayment($payment);
    $transaction = $instance->getTransaction();

    $payment_method = $payment->getPaymentMethod();
    $order = $payment->getOrder();

    $payment_method->getPaymentGateway();

    $username = $payment_method->get('oneclick_local_username')->value;
    $tbk_user = $payment_method->get('oneclick_tbk_user')->value;
    $payment->save();
    $details = [
      [
        'commerce_code' => $this->configuration['child_commerce_code'],
        'buy_order' => $payment->id(),
        'amount' => $payment->getAmount()->getNumber(),
        'installments_number' => 1,
      ],
    ];

    try {
      $response = $transaction->authorize($username, $tbk_user, $order->id(), $details);
      if ($response->isApproved()) {
        $details = $response->getDetails();
        $detail = end($details);

        $payment->setState('authorization');
        $payment->setRemoteId($detail->getAuthorizationCode());
        $payment->save();
        return;
      }
    }
    catch (MallTransactionAuthorizeException $e) {
    }

    throw new PaymentGatewayException('We encountered an unexpected error processing your payment method. Please try again later.');
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
    $instance = $this->getServiceInstanceFromPayment($payment);
    $amount = $amount ?: $payment->getAmount();
    $transaction = $instance->getTransaction();

    try {
      $response = $transaction->capture($this->configuration['child_commerce_code'], $payment->id(), $payment->getRemoteId(), $amount->getNumber());

      if ($response->getResponseCode() === 0) {
        $captured_amount = $response->getCapturedAmount();
        $payment->setAmount($captured_amount);
        $payment->setState('completed');
        $payment->save();
      }
      else {
        throw new PaymentGatewayException($this->t('Capture failed with code @code', ['@code' => $response->getResponseCode()]));
      }
    }
    catch (MallTransactionCaptureException $e) {
      throw new PaymentGatewayException($this->t('Capture failed: @reason', ['@reason' => Markup::create($e->getMessage())]), $e->getCode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
    $instance = $this->getServiceInstanceFromPayment($payment);
    $transaction = $instance->getTransaction();
    $order = $payment->getOrder();

    try {
      $response = $transaction->refund($order->id(), $this->configuration['child_commerce_code'], $payment->id(), $amount->getNumber());
      if ($response->getResponseCode() === 0) {
        $old_refunded_amount = $payment->getRefundedAmount();
        $new_refunded_amount = $old_refunded_amount->add($amount);
        if ($new_refunded_amount->lessThan($payment->getAmount())) {
          $payment->setState('partially_refunded');
        }
        else {
          $payment->setState('refunded');
        }
        $payment->setRefundedAmount($new_refunded_amount);
        $payment->save();
      }
      else {
        throw new PaymentGatewayException($this->t('Void failed with code @code', ['@code' => $response->getResponseCode()]));
      }
    }
    catch (MallRefundTransactionException $e) {
      throw new PaymentGatewayException($this->t('Void failed: @reason', ['@reason' => Markup::create($e->getMessage())]), $e->getCode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization', 'completed']);
    /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
    $instance = $this->getServiceInstanceFromPayment($payment);
    $transaction = $instance->getTransaction();
    $order = $payment->getOrder();

    $amount = $payment->getAmount();
    try {
      $response = $transaction->refund($order->id(), $this->configuration['child_commerce_code'], $payment->id(), $amount->getNumber());
      if ($response->getResponseCode() === 0) {
        $payment->setState('authorization_voided');
        $payment->save();
      }
      else {
        throw new PaymentGatewayException($this->t('Void failed with code @code', ['@code' => $response->getResponseCode()]));
      }
    }
    catch (MallRefundTransactionException $e) {
      throw new PaymentGatewayException($this->t('Void failed: @reason', ['@reason' => Markup::create($e->getMessage())]), $e->getCode());
    }
  }

  /**
   * From the payment method get the service entity and its plugin instance.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   *
   * @return \Drupal\transbank\TransbankServiceTypeInterface
   *   The plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getServiceInstanceFromPaymentMethod(PaymentMethodInterface $payment_method) {
    $configuration = $payment_method->getPaymentGateway()->getPluginConfiguration();
    $service_id = $configuration['service'] ?? NULL;

    if (!$service_id || !($service = Service::load($service_id))) {
      throw new \InvalidArgumentException('The service does not exists.');
    }

    return $service->getServiceInstance();
  }

  /**
   * From the payment get the service entity and its plugin instance.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @return \Drupal\transbank\TransbankServiceTypeInterface
   *   The plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getServiceInstanceFromPayment(PaymentInterface $payment) {
    $configuration = $payment->getPaymentGateway()->getPluginConfiguration();
    $service_id = $configuration['service'] ?? NULL;

    if (!$service_id || !($service = Service::load($service_id))) {
      throw new \InvalidArgumentException('The service does not exists.');
    }

    return $service->getServiceInstance();
  }

}
