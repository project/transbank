<?php

namespace Drupal\transbank_oneclick\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the oneclick payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "transbank_oneclick",
 *   label = @Translation("Transbank Oneclick"),
 * )
 */
class Oneclick extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@card_number' => $payment_method->get('oneclick_card_number')->value,
    ];
    return $this->t('Oneclick ending in @card_number', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['oneclick_local_username'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Local username'))
      ->setDescription($this->t('The original username.'))
      ->setRequired(TRUE);

    $fields['oneclick_card_type'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Card type'))
      ->setDescription($this->t('The credit card type.'))
      ->setRequired(TRUE);

    $fields['oneclick_card_number'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Card number'))
      ->setDescription($this->t('The last few digits of the credit card number'))
      ->setRequired(TRUE);

    $fields['oneclick_authorization_code'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Authorization code'));

    $fields['oneclick_tbk_user'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Transbank user'))
      ->setRequired(TRUE);

    return $fields;
  }

}
