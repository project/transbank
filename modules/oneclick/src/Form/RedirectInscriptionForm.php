<?php

namespace Drupal\transbank_oneclick\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\transbank\Entity\Service;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Transbank\Webpay\Oneclick\Exceptions\InscriptionStartException;

/**
 * The form that redirects the user to create their inscription in Oneclick.
 */
class RedirectInscriptionForm extends FormBase {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected Session $session;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->session = $container->get('session');
    $instance->session = $container->get('session');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'transbank_oneclick_redirect_inscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $this->session->get('oneclick_response_url');

    if (!isset($values['response_url']) || !isset($values['service_id'])) {
      $this->redirectUser();
    }

    $service_id = $values['service_id'];
    /** @var \Drupal\transbank\ServiceInterface $service */
    if (!($service = Service::load($service_id))) {
      $this->redirectUser('The service does not exist.');
    }

    /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
    $instance = $service->getServiceInstance();

    $inscription = $instance->getInscription();

    $username = $this->currentUser()->getAccountName();
    $email = $this->currentUser()->getEmail();

    try {
      $response = $inscription->start($username, $email, $values['response_url']);

      $form['#action'] = $response->getUrlWebpay();
      $form['TBK_TOKEN'] = [
        '#type' => 'hidden',
        '#value' => $response->getToken(),
      ];
      $form['info'] = [
        '#markup' => $this->t('You will be redirected to the Oneclick portal to register your payment method.'),
      ];

      $form['actions'] = [
        '#type' => 'actions',
      ];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Create the inscription'),
      ];
    }
    catch (GuzzleException | InscriptionStartException $e) {
      $form['error'] = [
        '#markup' => $this->t('An unexpected error has occurred, please try again later.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Redirect the user to the payment collection.
   *
   * @param string|\Drupal\Core\StringTranslation\TranslatableMarkup $message
   *   The message or null.
   */
  protected function redirectUser($message = NULL) {
    if (is_string($message) || $message instanceof TranslatableMarkup) {
      $this->messenger()->addWarning($message);
    }
    $url = Url::fromRoute('entity.commerce_payment_method.collection', [
      'user' => $this->currentUser()->id(),
    ]);

    $response = new RedirectResponse($url->toString());
    $response->send();
  }

}
