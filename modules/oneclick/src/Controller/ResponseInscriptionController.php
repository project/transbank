<?php

namespace Drupal\transbank_oneclick\Controller;

use Drupal\commerce_payment\Controller\PaymentCheckoutController;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\transbank\Entity\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Transbank\Webpay\Oneclick\Exceptions\InscriptionFinishException;

/**
 * Provides response endpoints for oneclick inscription.
 */
class ResponseInscriptionController extends PaymentCheckoutController {

  /**
   * The payment method storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $paymentMethodStorage;

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected Session $session;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->paymentMethodStorage = $container->get('entity_type.manager')->getStorage('commerce_payment_method');
    $instance->session = $container->get('session');
    $instance->currentUser = $container->get('current_user');

    return $instance;
  }

  /**
   * Create the oneclick payment method.
   *
   * After that, it redirects the user to their payment methods.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $payment_gateway
   *   The payment gateway id.
   * @param string $payment_method_type
   *   The payment method type.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addPaymentMethod(Request $request, $payment_gateway, $payment_method_type) {
    $this->createPaymentMethod($request, $payment_gateway, $payment_method_type);

    $url = Url::fromRoute('entity.commerce_payment_method.collection', [
      'user' => $this->currentUser->id(),
    ]);

    return new RedirectResponse($url->toString());
  }

  /**
   * Create the oneclick payment method from a checkout.
   *
   * After that, it redirects the user to their payment methods.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addOnCheckout(Request $request, RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    $step_id = $route_match->getParameter('step');

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    if ($payment_gateway_plugin->getPluginId() != 'transbank_oneclick') {
      throw new AccessException('The payment gateway for the order is not Oneclick');
    }

    $payment_method = $this->createPaymentMethod($request, $payment_gateway->id(), 'transbank_oneclick');

    if ($payment_method) {
      $order->set('payment_method', $payment_method);
      $order->save();
    }
    $url = Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $order->id(),
      'step' => $step_id,
    ]);

    return new RedirectResponse($url->toString());
  }

  /**
   * A helper function to create a payment method.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $payment_gateway
   *   The payment gateway id.
   * @param string $payment_method_type
   *   The payment method type.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentMethodInterface|null
   *   The payment method created or null.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createPaymentMethod(Request $request, $payment_gateway, $payment_method_type) {
    $token = $request->query->get('TBK_TOKEN');

    if ($token) {
      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $payment_method = $this->paymentMethodStorage->create([
        'type' => $payment_method_type,
        'payment_gateway' => $payment_gateway,
        'uid' => $this->currentUser->id(),
      ]);

      $payment_gateway = $payment_method->getPaymentGateway();

      $payment_gateway_plugin = $payment_gateway->getPlugin();
      $service_id = $payment_gateway_plugin->getConfiguration()['service'];

      /** @var \Drupal\transbank\ServiceInterface $service */
      if (!($service = Service::load($service_id))) {
        throw new NotFoundHttpException('The service does not exist.');
      }

      /** @var \Drupal\transbank_oneclick\Plugin\TransbankServiceType\OneclickInterface $instance */
      $instance = $service->getServiceInstance();
      try {
        $response = $instance->getInscription()->finish($token);
        $payment_method->set('oneclick_local_username', $this->currentUser->getAccountName());
        $payment_method->set('oneclick_card_type', $response->getCardType());
        $payment_method->set('oneclick_card_number', $response->getCardNumber());
        $payment_method->set('oneclick_authorization_code', $response->getAuthorizationCode());
        $payment_method->set('oneclick_tbk_user', $response->getTbkUser());
        $payment_method->save();

        $this->messenger->addStatus('The registration of your card in Oneclick has been created successfully.');
      }
      catch (InscriptionFinishException $e) {
        $this->messenger->addError('The registration of your card in Oneclick has failed.');

        return NULL;
      }
    }
    $this->session->remove('oneclick_response_url');

    return $payment_method;
  }

}
