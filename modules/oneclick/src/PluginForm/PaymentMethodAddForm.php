<?php

namespace Drupal\transbank_oneclick\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Url;
use Drupal\transbank\Entity\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Provides the add form for oneclick payment method.
 */
class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * The service storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $serviceStorage;

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected Session $session;

  /**
   * The current route match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->currentUser = $container->get('current_user');
    $instance->session = $container->get('session');
    $instance->serviceStorage = $container->get('entity_type.manager')->getStorage('transbank_service');
    $instance->routeMatch = $container->get('current_route_match');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['payment_details'] = $this->buildOneclickForm($form['payment_details'], $form_state);

    return $form;
  }

  /**
   * Build the oneclick form.
   *
   * @param array $element
   *   The sub element from the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element.
   */
  protected function buildOneclickForm(array $element, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_gateway = $payment_method->getPaymentGateway();
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $service_id = $payment_gateway_plugin->getConfiguration()['service'];

    /** @var \Drupal\transbank\ServiceInterface $service */
    if (!Service::load($service_id)) {
      throw new \InvalidArgumentException('The service does not exist.');
    }

    if ($this->routeMatch->getRouteName() == 'commerce_checkout.form') {
      $url = Url::fromRoute('transbank_oneclick.commerce_payment.response_add_payment_method_on_checkout', [
        'commerce_order' => $this->routeMatch->getRawParameter('commerce_order'),
        'step' => $this->routeMatch->getRawParameter('step'),
      ]);
    }
    else {
      $url = Url::fromRoute('transbank_oneclick.commerce_payment.response_add_payment_method', [
        'payment_gateway' => $payment_gateway->id(),
        'payment_method_type' => $payment_method->bundle(),
      ]);
    }

    $this->session->set('oneclick_response_url', [
      'response_url' => $url->setAbsolute()->toString(),
      'service_id' => $service_id,
    ]);

    $element['info'] = [
      '#markup' => $this->t('You will be redirected to a form to register your payment method with Oneclick'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;
    $payment_gateway = $payment_method->getPaymentGateway();

    /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
    $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');

    $reusable = $payment_method_storage->loadReusable($payment_method->getOwner(), $payment_gateway);

    if (!empty($reusable)) {
      $form_state->setErrorByName('', $this->t('You already have a Oneclick registration.'));
    }
    else {
      if ($form_state->getTriggeringElement()['#name'] == 'op') {
        // With this we stop the parent submit callback.
        $form_state->setErrorByName('', '');

        $url = Url::fromRoute('transbank_oneclick.commerce_payment.redirect_inscription_form');
        $response = new RedirectResponse($url->toString());
        $response->send();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

}
